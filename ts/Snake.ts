/**
 * Created by Matthijs on 11-6-2019.
 */
class Snake {
    private _snake: Array<Snakepart>;

    constructor(){
        this._snake = [
            new Snakepart(150, 150),
            new Snakepart(140, 150),
            new Snakepart(130, 150),
            new Snakepart(120, 150),
            new Snakepart(110, 150)]
    }

    get snake(){
        return this._snake;
    }

}