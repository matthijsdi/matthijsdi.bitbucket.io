class Game {
    public element = <HTMLCanvasElement> document.getElementById('game');
    public ctx = this.element.getContext("2d");
    private _snake: Snake;
    private speedX: number;
    private speedY: number;
    private foodX: number;
    private foodY: number;
    private DEATH_BLOCKS: number = 10;
    private deathX : Array<number>  = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100];
    private deathY : Array<number>  = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100];

    constructor() {
        // this._element = document.getElementById('game') as HTMLCanvasElement;
        this._snake = new Snake();
        window.addEventListener('keydown', this.changeDirection);
        this.speedY = 0;
        this.speedX = 10;
    }

    public main() {
        if (this.didGameEnd()) {
            alert('Gameover')
        } else {

            setTimeout(function onTick() {

                this.game.clearCanvas();
                this.game.drawFood();
                this.game.drawDeath();
                this.game.advanceSnake();
                this.game.drawSnake();

                this.game.main();
            }, 100)
        }
    }

    public changeDirection = (e: KeyboardEvent) => {
        const goingUp = this.speedY === -10;
        const goingDown = this.speedY === 10;
        const goingRight = this.speedX === 10;
        const goingLeft = this.speedX === -10;

        const keyPressed = e.keyCode;

        if (keyPressed === 37 && !goingRight) {
            this.speedX = -10;
            this.speedY = 0;
        }
        if (keyPressed === 38 && !goingDown) {
            this.speedX = 0;
            this.speedY = -10;
        }
        if (keyPressed === 39 && !goingLeft) {
            this.speedX = 10;
            this.speedY = 0;
        }
        if (keyPressed === 40 && !goingUp) {
            this.speedX = 0;
            this.speedY = 10;
        }
    }

    public clearCanvas() {
        this.ctx.fillStyle = 'white';
        this.ctx.strokeStyle = 'black';

        this.ctx.fillRect(0, 0, this.element.width, this.element.height);
        this.ctx.strokeRect(0, 0, this.element.width, this.element.height);
    }

    public advanceSnake() {
        var head = new Snakepart(this._snake.snake[0].X + this.speedX, this._snake.snake[0].Y + this.speedY);

        this._snake.snake.unshift(head);

        if (this._snake.snake[0].X === this.foodX && this._snake.snake[0].Y === this.foodY) {
            this.createFood();
            this.createDeath();
        } else {
            this._snake.snake.pop();
        }
    }

    public drawSnake() {
        this._snake.snake.forEach(this.drawSnakePart)
    }

    public drawSnakePart(snakePart: Snakepart) {
        var element = <HTMLCanvasElement> document.getElementById('game');
        var ctx = element.getContext("2d");
        ctx.fillStyle = ctx.strokeStyle = 'blue';

        ctx.fillRect(snakePart.X, snakePart.Y, 10, 10);
        ctx.strokeRect(snakePart.X, snakePart.Y, 10, 10);
    }

    public createFood() {
        var max = 0;
        var minX = this.element.width -10;
        var minY = this.element.height -10;

        var foodX = Math.round((Math.random() * (max-minX) + minX) / 10) * 10;

        var foodY = Math.round((Math.random() * (max-minY) + minY) / 10) * 10;

        // this._snake.snake.forEach(function isFoodOnSnake(part : Snakepart) {
        //     var foodIsoNsnake = part.X == foodX && part.Y == foodY;
        //     if (foodIsoNsnake) createFood();
        // });

        this.foodX = foodX;
        this.foodY = foodY;
    }

    public drawFood() {
        this.ctx.fillStyle = 'yellow';
        this.ctx.strokeStyle = 'black';

        this.ctx.fillRect(this.foodX, this.foodY, 10, 10);
        this.ctx.strokeRect(this.foodX, this.foodY, 10, 10);
    }

    public createDeath() {
        for (let i = 0; i < this.DEATH_BLOCKS; i++) {
            var max = 0;
            var minX = this.element.width - 10;
            var minY = this.element.height - 10;

            var deathX = Math.round((Math.random() * (max - minX) + minX) / 10) * 10;
            var deathY = Math.round((Math.random() * (max - minY) + minY) / 10) * 10;

            // this._snake.snake.forEach(function isDeathOnSnake(part: Snakepart) {
            //     if (part.X == deathX && part.Y == deathY || part.X == this.foodX && part.Y == foodY) {
            //     this.game.createDeath();
            //     }
            // });
            this.deathX[i] = deathX;
            this.deathY[i] = deathY;
            this.drawDeath()
        }
    }

    public drawDeath() {
        for (let i=0; i < this.DEATH_BLOCKS; i++) {
            this.ctx.fillStyle = 'pink';
            this.ctx.strokeStyle = 'black';

            this.ctx.fillRect(this.deathX[i], this.deathY[i], 10, 10);
            this.ctx.strokeRect(this.deathX[i], this.deathY[i], 10, 10);
        }
    }

    public didGameEnd() {
        for (let i = 4; i < this._snake.snake.length; i++) {
            if (// Raakt de slang zichzelf?
            this._snake.snake[i].X === this._snake.snake[0].X && this._snake.snake[i].Y === this._snake.snake[0].Y
            // Raakt de slang de linker of de bovenste muur?
            || this._snake.snake[0].X < 0                       || this._snake.snake[0].Y < 0
            // Raakt de slang de rechter of onderste muur?
            || this._snake.snake[0].X > this.element.width - 10   || this._snake.snake[0].Y > this.element.height - 10){
                return true;
            } else {
                for(let i = 0; i<this.DEATH_BLOCKS; i++){
                    if (this._snake.snake[0].X == this.deathX[i] && this._snake.snake[0].Y == this.deathY[i]){
                        return true;
                    }
                }
                return false;
            }
        } return false;
    }
}