class Game {
    constructor() {
        this.element = document.getElementById('game');
        this.ctx = this.element.getContext("2d");
        this.DEATH_BLOCKS = 10;
        this.deathX = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100];
        this.deathY = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100];
        this.changeDirection = (e) => {
            const goingUp = this.speedY === -10;
            const goingDown = this.speedY === 10;
            const goingRight = this.speedX === 10;
            const goingLeft = this.speedX === -10;
            const keyPressed = e.keyCode;
            if (keyPressed === 37 && !goingRight) {
                this.speedX = -10;
                this.speedY = 0;
            }
            if (keyPressed === 38 && !goingDown) {
                this.speedX = 0;
                this.speedY = -10;
            }
            if (keyPressed === 39 && !goingLeft) {
                this.speedX = 10;
                this.speedY = 0;
            }
            if (keyPressed === 40 && !goingUp) {
                this.speedX = 0;
                this.speedY = 10;
            }
        };
        this._snake = new Snake();
        window.addEventListener('keydown', this.changeDirection);
        this.speedY = 0;
        this.speedX = 10;
    }
    main() {
        if (this.didGameEnd()) {
            alert('Gameover');
        }
        else {
            setTimeout(function onTick() {
                this.game.clearCanvas();
                this.game.drawFood();
                this.game.drawDeath();
                this.game.advanceSnake();
                this.game.drawSnake();
                this.game.main();
            }, 100);
        }
    }
    clearCanvas() {
        this.ctx.fillStyle = 'white';
        this.ctx.strokeStyle = 'black';
        this.ctx.fillRect(0, 0, this.element.width, this.element.height);
        this.ctx.strokeRect(0, 0, this.element.width, this.element.height);
    }
    advanceSnake() {
        var head = new Snakepart(this._snake.snake[0].X + this.speedX, this._snake.snake[0].Y + this.speedY);
        this._snake.snake.unshift(head);
        if (this._snake.snake[0].X === this.foodX && this._snake.snake[0].Y === this.foodY) {
            this.createFood();
            this.createDeath();
        }
        else {
            this._snake.snake.pop();
        }
    }
    drawSnake() {
        this._snake.snake.forEach(this.drawSnakePart);
    }
    drawSnakePart(snakePart) {
        var element = document.getElementById('game');
        var ctx = element.getContext("2d");
        ctx.fillStyle = ctx.strokeStyle = 'blue';
        ctx.fillRect(snakePart.X, snakePart.Y, 10, 10);
        ctx.strokeRect(snakePart.X, snakePart.Y, 10, 10);
    }
    createFood() {
        var max = 0;
        var minX = this.element.width - 10;
        var minY = this.element.height - 10;
        var foodX = Math.round((Math.random() * (max - minX) + minX) / 10) * 10;
        var foodY = Math.round((Math.random() * (max - minY) + minY) / 10) * 10;
        this.foodX = foodX;
        this.foodY = foodY;
    }
    drawFood() {
        this.ctx.fillStyle = 'yellow';
        this.ctx.strokeStyle = 'black';
        this.ctx.fillRect(this.foodX, this.foodY, 10, 10);
        this.ctx.strokeRect(this.foodX, this.foodY, 10, 10);
    }
    createDeath() {
        for (let i = 0; i < this.DEATH_BLOCKS; i++) {
            var max = 0;
            var minX = this.element.width - 10;
            var minY = this.element.height - 10;
            var deathX = Math.round((Math.random() * (max - minX) + minX) / 10) * 10;
            var deathY = Math.round((Math.random() * (max - minY) + minY) / 10) * 10;
            this.deathX[i] = deathX;
            this.deathY[i] = deathY;
            this.drawDeath();
        }
    }
    drawDeath() {
        for (let i = 0; i < this.DEATH_BLOCKS; i++) {
            this.ctx.fillStyle = 'pink';
            this.ctx.strokeStyle = 'black';
            this.ctx.fillRect(this.deathX[i], this.deathY[i], 10, 10);
            this.ctx.strokeRect(this.deathX[i], this.deathY[i], 10, 10);
        }
    }
    didGameEnd() {
        for (let i = 4; i < this._snake.snake.length; i++) {
            if (this._snake.snake[i].X === this._snake.snake[0].X && this._snake.snake[i].Y === this._snake.snake[0].Y
                || this._snake.snake[0].X < 0 || this._snake.snake[0].Y < 0
                || this._snake.snake[0].X > this.element.width - 10 || this._snake.snake[0].Y > this.element.height - 10) {
                return true;
            }
            else {
                for (let i = 0; i < this.DEATH_BLOCKS; i++) {
                    if (this._snake.snake[0].X == this.deathX[i] && this._snake.snake[0].Y == this.deathY[i]) {
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }
}
class Snake {
    constructor() {
        this._snake = [
            new Snakepart(150, 150),
            new Snakepart(140, 150),
            new Snakepart(130, 150),
            new Snakepart(120, 150),
            new Snakepart(110, 150)
        ];
    }
    get snake() {
        return this._snake;
    }
}
class Snakepart {
    constructor(x, y) {
        this._x = x;
        this._y = y;
    }
    get X() {
        return this._x;
    }
    get Y() {
        return this._y;
    }
    set X(x) {
        this._x = x;
    }
    set Y(y) {
        this._y = y;
    }
}
//# sourceMappingURL=main.js.map